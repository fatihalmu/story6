from django.db import models


# Create your models here.
class Status(models.Model):
	name = models.TextField(max_length=100)
	status = models.TextField(max_length=300)

