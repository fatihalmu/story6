from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
class Lab6Test(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code,200)
	def test_lab_6_using_to_do_list_template(self):
		response = Client().get('/story6/')
		self.assertTemplateUsed(response, 'landingpage.html')
	def test_lab_6_using_index_func(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, home)
	def test_model_can_create_new_status(self):
		new_status = Status.objects.create(name ="fatih",status="ppw,tolong jelasin dikelas")
		#Retrieving all available activity
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)

class Lab6FuncTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		serice_args = ['--verbose']
		self.driver  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab6FuncTest, self).setUp()
	def tearDown(self):
		self.driver.close()
		super(Lab6FuncTest, self).tearDown()
	def test_input_status(self):
		browser =self.driver
		browser.get("http://127.0.0.1:8000/")
		self.assertIn("Story 6", browser.title)	
		elem_nama = browser.find_element_by_id("label_nama")
		elem_status = browser.find_element_by_id("label_status")
		elem_nama.send_keys("selenium")
		elem_status.send_keys("Coba Coba")
		elem_status.send_keys(Keys.RETURN)
		assert "Coba Coba" in browser.page_source