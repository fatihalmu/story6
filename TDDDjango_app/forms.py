from django import forms
from .models import Status

class statusku(forms.ModelForm):
	class Meta:
		model = Status

		fields = ['name','status']
		
		widgets ={
		'name': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi nama','id':'label_nama'}),
		'status': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi status','id':'label_status'}),
		}

		labels = {
            'name': 'Nama',
            'status': 'Status',
        }