from django.shortcuts import render
from .forms import statusku
from .models import Status
from django.shortcuts import reverse
from django.http import HttpResponse,HttpResponseRedirect

# Create your views here.
mystatus = {}
def home(request):

	if request.method == "POST":
		form = statusku(request.POST)
		if form.is_valid():
			statusbaru = form.save()
			statusbaru.save()
			return HttpResponseRedirect(reverse('beranda'))
		else:
			pass
	else:
		form = statusku()
		results =  Status.objects.all()
		return render(request, 'landingpage.html', {'daftarstatus': results,'form':form})
