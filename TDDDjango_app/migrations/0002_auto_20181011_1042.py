# Generated by Django 2.1.1 on 2018-10-11 03:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TDDDjango_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='status',
            old_name='Status',
            new_name='status',
        ),
    ]
