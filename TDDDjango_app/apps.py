from django.apps import AppConfig


class TdddjangoAppConfig(AppConfig):
    name = 'TDDDjango_app'
